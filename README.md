# MD-MC

Direct and MC combination simulations. Code, raw data an plots used for publishing direct simulations of Bose-Einstein-Condensates.

# How does it work? 

The workings are quite simple: 

1.) Simulate a MC simulation to obtain an equilibrium example 
2.) Simulate your trajectory

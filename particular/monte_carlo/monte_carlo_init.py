import numpy as np
from amarium import prepare_file_name_saving
from particular.monte_carlo.initialization import InitMonteCarlo, InitVelocity
from particular.physics.potentials.potentials import energy_gravity


def init_lj(
    sigma: float,
    epsilon: float,
    number_particles: int,
    box_len: float,
    file_name: str,
    dir_name: str,
) -> None:

    constants = [sigma, epsilon]
    N = number_particles
    L = box_len
    
    positions = InitMonteCarlo(N, L, constants)
    assert len(positions) == 128, "Added too less particles"
    file_name = prepare_file_name_saving(
        suffix=".npy", file_name=file_name, prefix=dir_name
    )
    np.save(arr=positions, file=file_name)


def init_gravity(N=128, L=8, T0=2):
    """
    Intialize gravity MC

    Args:
        N (int, optional): [description]. Defaults to 128.
        L (int, optional): [description]. Defaults to 512.
        T0 ([type], optional): [description]. Defaults to 2K.
    """
    G = G = 9.81
    constants = [G]
    positions = InitMonteCarlo(N, L, constants, energy=energy_gravity, tol=10 ** (3))
    assert len(positions) == N, "Added too less particles"
    np.save(arr=positions, file="Simulations/MCInit/pos_grav.npy")
    vels = InitVelocity(N=N, T0=2)
    assert len(vels) == N, "Added too less particles"
    file_name = prepare_file_name_saving(
        suffix="npy", file_name="vels_grav", prefix="Simulations/MCInit"
    )
    np.save(arr=vels, file="Simulations/MCInit/vels_grav.npy")


if __name__ == "__main__":
    
    number_particles = 128
    epsilon = 0.25
    sigma = 0.25 
    L = 8
    file_name = f"LJ_pos_{number_particles}"
    dir_name = "Simulations/MCInint"
    
    init_lj(sigma = 0.25, 
            epsilon = 0.25, 
            number_particles = number_particles, 
            box_len = 8, 
            file_name = file_name, 
            dir_name = dir_name)

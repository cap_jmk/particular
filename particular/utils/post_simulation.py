import numpy as np
from amarium import prepare_file_name_saving

def save_trajectories(arrays, prefix=""):
    """saves the trajectories

    Args:
        prefix ([type]): to path
        arrays ([type]): collection of data points
    """

    grid, positions, velocities, E_pot = arrays

    np.save(arr=grid, file=prepare_file_name_saving(prefix = prefix, file_name = "_grid.npy"))
    np.save(arr=positions, file=prepare_file_name_saving(prefix = prefix, file_name = "_pos.npy"))
    np.save(arr=velocities, file=prepare_file_name_saving(prefix = prefix, file_name = "_vel.npy"))
    np.save(arr=E_pot, file=prepare_file_name_saving(prefix = prefix, file_name = "_e_pot.npy"))

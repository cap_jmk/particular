import numpy as np
import warnings
from numba import njit


def load_initials(pos_file:str, vel_file:str):
    """Load initial conditions from another trajectory

    Args:
        prefix ([type]): [description]

    Returns:
        [type]: [description]
    """
    pos = load_traj(pos_file)
    vels = load_traj(vel_file)
    return [pos, vels]


def load_traj(path):
    """path to trajectory

    Args:
        path (string): string
    """
    pos = np.load(path)
    try:
        return pos[:, :, -1]
    except:
        warnings.warn("Loading an initial configuation not a trajectory")
        return pos
